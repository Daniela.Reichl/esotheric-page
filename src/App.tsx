import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Layout } from "./component/common/Layout";
import { HomeView } from "./component/main/Home/HomeView";
import { ServiceView } from "./component/main/Service/ServiceView";
import { ContactView } from "./component/main/Contact/ContactView";
import { Impressum } from "./component/footer/Impressum";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<HomeView />} />
            <Route path="services" element={<ServiceView />} />
            <Route path="contact" element={<ContactView />} />
            <Route path="impressum" element={<Impressum />} />
            {/* <Route path="*" element={<NoPage />} /> */}
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
