import React from "react";

export const Hero = () => {
  return (
    <div className="relative flex justify-center items-center border-t-4 border-b-4 border-gold">
      <img
        src="./src/assets/hero.avif"
        alt="hero"
        className="h-96 w-full object-cover"
      />
      <h2 className="bg-white -skew-x-12 absolute py-4 px-16 border-4 border-gold">
        <span className="font-AlumniSans underline text-gold text-2xl md:text-4xl lg:text-6xl font-bold">
          MEIN FREIRAUM
        </span>
      </h2>
    </div>
  );
};
