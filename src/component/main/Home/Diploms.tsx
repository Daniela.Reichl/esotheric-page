import React from "react";

export const Diploms = () => {
  return (
    <div className="flex flex-col gap-10 justify-center items-center my-10 mx-5">
      <h1 className="text-center">Ausbildung</h1>
      <div className="flex flex-col md:flex-row gap-4 w-full md:w-2/3">
        <div className="flex flex-col gap-5">
          <div>
            <h2>Ausbildung 1</h2>
            <p>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero eos et accusam et justo duo
              dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
              sanctus est Lorem ipsum dolor sit amet.
            </p>
          </div>
          <div>
            <h2>Ausbildung 2</h2>
            <p>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero eos et accusam et justo duo
              dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
              sanctus est Lorem ipsum dolor sit amet.
            </p>
          </div>
          <div>
            <h2>Ausbildung 3</h2>
            <p>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero eos et accusam et justo duo
              dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
              sanctus est Lorem ipsum dolor sit amet.
            </p>
          </div>
        </div>
        <img
          src="./src/assets/diplom_urkunde_muster.jpg"
          alt="bild"
          className="h-96"
        />
      </div>
    </div>
  );
};
