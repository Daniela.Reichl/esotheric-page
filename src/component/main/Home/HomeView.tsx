import { AboutMe } from "./AboutMe";
import { Hero } from "./Hero";
import { Diploms } from "./Diploms";
import { Philosophie } from "./Philosophie";

export const HomeView = () => {
  return (
    <div>
      <Hero />
      <AboutMe />
      <Philosophie />
      <Diploms />
    </div>
  );
};
