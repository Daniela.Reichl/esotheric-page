export const CranioSacral = () => {
  return (
    <div className="mainBox">
      <div className="textBox">
        <h2>Cranio Sacral</h2>
        <p className="md:w-96">
          Cranio-Sacral-Therapie ist eine sanfte, ganzheitliche
          Behandlungsmethode, die sich auf den Rhythmus der Gehirn- und
          Rückenmarksflüssigkeit konzentriert. Durch sanfte Berührungen und
          Manipulationen am Schädel und am Kreuzbein sollen Blockaden im
          craniosacralen System gelöst werden. Dies unterstützt die natürliche
          Selbstheilung des Körpers, lindert Spannungen und fördert das
          allgemeine Wohlbefinden. Die Therapie wird in der alternativen Medizin
          und der Wellness-Praxis angewandt, um körperliche und emotionale
          Blockaden zu lösen und das körperliche und seelische Gleichgewicht
          wiederherzustellen.
        </p>
      </div>
      <div className="pictureBackground">
        <img
          src="./src/assets/craniosacraltherapie.jpg"
          alt="Cranio Sacral Therapie"
          className="pictures"
        />
      </div>
    </div>
  );
};
