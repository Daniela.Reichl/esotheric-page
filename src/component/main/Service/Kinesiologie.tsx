export const Kinesiologie = () => {
  return (
    <div className="mainBox">
      <div className="textBox">
        <h2>Kinesiologie</h2>
        <p className="md:w-96">
          Die Kinesiologie ist eine alternative Gesundheitspraxis, die sich auf
          den Zusammenhang zwischen Muskeln, Energiefluss und emotionaler
          Gesundheit konzentriert. Durch manuelle Muskeltests sollen Blockaden
          im Körper identifiziert und behandelt werden. Diese Methode soll das
          körperliche, geistige und emotionale Gleichgewicht wiederherstellen
          und Stress abbauen. Die Kinesiologie wird in der Naturheilkunde und
          Wellness eingesetzt, um die Selbstheilungskräfte zu aktivieren und das
          Wohlbefinden zu steigern.
        </p>
      </div>
      <div className="pictureBackground">
        <img
          src="./src/assets/kinesiologie.jpg"
          alt="Kinesiologie"
          className="pictures"
        />
      </div>
    </div>
  );
};
