export const HolisticPulsing = () => {
  return (
    <div className="mainBoxReverse">
      <div className="pictureBackground">
        <img
          src="./src/assets/HolisticPulsing.png"
          alt="HolisticPulsing"
          className="pictures"
        />
      </div>
      <div className="textBox">
        <h2>Holistic Pulsing</h2>
        <p className="md:w-96">
          Holistic Pulsing ist eine ganzheitliche Heilmethode, die auf sanften,
          rhythmischen Bewegungen des Körpers basiert. Dabei werden
          Flüssigkeiten im Gewebe stimuliert, um Verspannungen zu lösen und den
          Energiefluss zu fördern. Dies soll physische und emotionale Blockaden
          auflösen, Entspannung fördern und das allgemeine Wohlbefinden
          steigern. Holistic Pulsing wird in der alternativen Medizin und
          Wellness-Praktiken verwendet, um Körper, Geist und Seele in Einklang
          zu bringen.
        </p>
      </div>
    </div>
  );
};
