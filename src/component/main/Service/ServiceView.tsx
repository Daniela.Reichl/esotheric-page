import { HolisticPulsing } from "./HolisticPulsing";
import { Kinesiologie } from "./Kinesiologie";
import { CranioSacral } from "./CranioSacral";
import { Spagyrik } from "./Spagyrik";
import { FadeIn } from "../../common/FadeIn";

export const ServiceView = () => {
  return (
    <div className="textBox">
      <FadeIn>
        <Kinesiologie />
      </FadeIn>
      <FadeIn>
        <HolisticPulsing />
      </FadeIn>
      <FadeIn>
        <CranioSacral />
      </FadeIn>
      <FadeIn>
        <Spagyrik />
      </FadeIn>
    </div>
  );
};
