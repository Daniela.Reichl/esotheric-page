export const Spagyrik = () => {
  return (
    <div className="mainBoxReverse">
      <div className="pictureBackground">
        <img
          src="./src/assets/spagyrik.jpg"
          alt="HolisticPulsing"
          className="pictures"
        />
      </div>
      <div className="textBox">
        <h2>Spagyrik</h2>
        <p className="md:w-96">
          Die Spagyrik ist eine holistische Heilmethode, die auf Prinzipien der
          Alchemie basiert. Sie verwendet pflanzliche Extrakte, um Körper, Geist
          und Seele zu behandeln. Dabei werden Pflanzenextrakte destilliert,
          verascht und wieder vereint, um ihre reinigende und energetische
          Wirkung zu verstärken. Die Spagyrik zielt darauf ab, das Gleichgewicht
          im Organismus wiederherzustellen, Toxine zu eliminieren und die
          Selbstheilungskräfte zu aktivieren. Diese Methode wird in der
          Naturheilkunde und alternativen Medizin angewandt, um ganzheitliche
          Gesundheit und Wohlbefinden zu fördern.
        </p>
      </div>
    </div>
  );
};
