export const ContactField = () => {
  return (
    <div className="flex flex-col gap-4 justify-center bg-turquoise shadow-xl text-white text-lg p-10 h-72 w-72 md:w-80 rounded-t-xl rounded-l-none md:rounded-r-none md:rounded-l-xl">
      <div>
        <p className="font-bold">Max Mustermann</p>
        <p>Musterstraße 15</p>
        <p>9020 Klagenfurt am Wörthersee</p>
      </div>
      <div>
        <p>0660/XXX XXXX</p>
        <p> max.Mustermann@gmail.com</p>
      </div>
    </div>
  );
};
