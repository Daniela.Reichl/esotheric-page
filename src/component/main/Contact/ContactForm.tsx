export const ContactForm = () => {
  return (
    <form className="flex flex-col gap-5 p-8 w-[21rem] shadow-2xl border-2 rounded-xl bg-white ">
      <input
        type="text"
        name="name"
        id="name"
        placeholder="Vorname Nachname"
        className="focusBorder underlineContact"
      />
      <input
        type="email"
        name="email"
        id="email"
        placeholder="E-Mail Adresse"
        className="focusBorder underlineContact"
      />
      <textarea
        name="message"
        id="message"
        placeholder="Nachricht"
        className="h-32 w-72 scrollbar-thin scrollbar-thumb-gold scrollbar-track-white focusBorder underlineContact"
      ></textarea>
      <button
        type="submit"
        className="bg-gold py-2 px-5 rounded-full shadow-lg text-white font-bold w-fit"
      >
        Senden
      </button>
    </form>
  );
};
