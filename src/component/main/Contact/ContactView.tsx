import { ContactField } from "./ContactField";
import { ContactForm } from "./ContactForm";

export const ContactView = () => {
  return (
    <div className="my-8 mx-2 lg:mx-40">
      <h1 className="text-center">Kontaktformular</h1>
      <div className="flex flex-col md:flex-row justify-center items-center my-10">
        <ContactField />
        <ContactForm />
        <div className=" bg-turquoise shadow-xl h-10 md:h-72 w-72 md:w-10 rounded-b-xl md:rounded-l-none md:rounded-r-xl"></div>
      </div>
    </div>
  );
};
