import { useInView } from "react-intersection-observer";
import { ReactNode } from "react";

interface FadeInProps {
  children: ReactNode;
}

export const FadeIn: React.FC<FadeInProps> = ({ children }) => {
  const [ref, inView] = useInView({
    triggerOnce: true, // Only trigger once when in view
  });

  return (
    <div
      ref={ref}
      className={`transform ${
        inView ? "opacity-100 translate-y-0" : "opacity-0 translate-y-10"
      } transition duration-500`}
    >
      {children}
    </div>
  );
};
