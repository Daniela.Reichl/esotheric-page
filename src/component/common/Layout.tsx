import { Outlet } from "react-router-dom";
import { HeaderView } from "../header/HeaderView";
import { FooterView } from "../footer/FooterView";

export const Layout = () => {
  return (
    <section className="flex flex-col h-screen justify-between">
      <HeaderView />
      <Outlet />
      <FooterView />
    </section>
  );
};
