import { Link } from "react-router-dom";

export const HeaderView = () => {
  return (
    <div className="py-5 px-5 bg-turquoise text-white text-xl font-bold flex flex-col md:flex-row justify-between gap-5">
      <Link to="/">
        <img
          src="../src/assets/logoKlein.svg"
          alt="logo"
          className="h-10 md:h-16 hover:scale-105"
        />
      </Link>
      <nav>
        <ul className="flex items-center justify-between gap-10 h-full">
          <li className="hover:text-gold duration-500 ">
            <Link to="/">Home</Link>
          </li>
          <li className="hover:text-gold duration-500">
            <Link to="/services">Leistungen</Link>
          </li>
          <li className=" hover:text-gold duration-500">
            <Link to="/contact">Kontakt</Link>
          </li>
        </ul>
      </nav>
    </div>
  );
};
