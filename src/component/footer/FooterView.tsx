import { Link } from "react-router-dom";

export const FooterView = () => {
  return (
    <div className="flex flex-col justify-center items-center gap-5 py-2 bg-turquoise text-white">
      <Link to="/impressum" className="hover:font-bold">
        Impressum
      </Link>
      <p className="flex gap-2 items-center justify-center">
        made with
        <img src="./src/assets/heart.svg" alt="heart" className="h-8" /> in
        Klagenfurt - Austria
      </p>
    </div>
  );
};
