export const Impressum = () => {
  return (
    <div className="flex flex-col gap-10 mx-5 md:mx-20">
      <h2>Impressum</h2>
      <div className="flex flex-col gap-5 ">
        <p>
          <span className="label">Firmawortlaut:</span> Name
        </p>
        <p>
          <span className="label">Unternehmensgegenstand</span> Dienstleister
        </p>
        <p>
          <span className="label">UID-Nr:</span> ATU17401894
        </p>
        <p>
          <span className="label">Firmenbuchnummer</span> FN: 153546a
        </p>
        <p>
          <span className="label">Firmenbuchgericht</span> Landesgericht
          Klagenfurt
        </p>
        <p>
          <span className="label">Firmensitz</span> 9020 Klagenfurt am
          Wörthersee
        </p>
        <p>
          <span className="label">Anschrift </span>
          XXX, 9020 Klagenfurt am Wörthersee, Österreich
        </p>
        <p>
          <span className="label">Kontaktdaten</span> Tel: +43 680 XXX XXXXX
          E-Mail: X@muster.at
        </p>
        <p>
          <span className="label">Mitglied bei</span> Mitglied der
          Wirtschaftskammer Österreich
        </p>
        <p>
          <span className="label">Gewerbeordnung</span> www.ris.bka.gv.at
        </p>
        <p>
          <span className="label">Aufsichtsbehörde</span> Bezirkshauptmannschaft
          Klagenfurt a. Ws.
        </p>
        <div>
          <span className="label">Angaben zur Online-Streitbeilegung</span>{" "}
          Verbraucher haben die Möglichkeit, Beschwerden an die Online-
          Streitbeilegungsplattform der EU zu richten: http://ec.europa.eu/odr.
          <p>
            Sie können allfällige Beschwerde auch an die oben angegebene
            E-Mail-Adresse richten
          </p>
        </div>
      </div>
    </div>
  );
};
